<?php

/*
 +--------------------------------------------------------------------+
 | CiviCRM version 3.3                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2010
 * $Id$
 *
 */

require_once 'CRM/Core/Page.php';


class CRM_Invoice_Page_InvoiceSettings extends CRM_Core_Page
{
    /** 
     * @return void 
     * @access public 
     * 
     */ 
    function preProcess( ) 
    {
    
//        CRM_Utils_System::setTitle( 'VAT and Invoice Settings' );
        CRM_Utils_System::setTitle( 'Invoice Settings' );
        
        $query = "SELECT * FROM ".CIVICRM_INVOICE_SETTINGS_TABLE;
        $dao = CRM_Core_DAO::executeQuery( $query );
        
        require_once 'CRM/Contribute/PseudoConstant.php';
        $invoiceSettings = array();
        while ($dao->fetch ()) {
            $invoiceSettings[$dao->id]['id'] = $dao->id;
            $invoiceSettings[$dao->id]['financial_type_id'] = CRM_Contribute_PseudoConstant::financialType($dao->financial_type_id);
            $invoiceSettings[$dao->id]['calculate_vat'] = $dao->calculate_vat;
            $invoiceSettings[$dao->id]['create_pdf_invoice'] = $dao->create_pdf_invoice;
            //$invoiceSettings[$dao->id]['vat_percentage'] = $dao->vat_percentage;
            //$invoiceSettings[$dao->id]['vat_weighting'] = $dao->vat_weighting;
            //$invoiceSettings[$dao->id]['is_vat_inclusive'] = $dao->is_vat_inclusive;
        }
        
        $this->assign( 'invoiceSettingsArray', $invoiceSettings );
        $this->assign( 'invoiceSettingsCount', count($invoiceSettings) );
    }

    /** 
     * This function is the main function that is called when the page loads, 
     * it decides the which action has to be taken for the page. 
     *                                                          
     * return null        
     * @access public 
     */                                                          
    function run( ) { 
        $this->preProcess( );
        
        return parent::run( );
    }

}