{*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.1                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*}
<div class="crm-block crm-form-block crm-export-form-block">

<!--<div class="help">
<p>{ts}Use this form for Invoice Settings.{/ts}</p>
</div>-->

{if $action eq '1' OR $action eq '2'}
<div class="help">
    {ts}Use this form to add/edit VAT Code.{/ts}
</div>
<table class="form-layout">
     <tr>
		<td>
			<table class="form-layout">
             <!--sri code starts here on 9th May 2013 -->
                  
                  
                <tr>
                
                <tr>
                
                <tr id="vat_rows">
                    <td>
                        
                        <table class="form-layout">
                            
							
                              <tr id="vat_code_row">
                            <td class="label">{$form.vat_code.label}</td>
                            <td>{$form.vat_code.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the VAT Code. Ex. Standard, Reduced.{/ts}
        		                </span>
                            </td>
                        </tr>
							<tr id="vat_rate_row">
                            <td class="label">{$form.vat_rate.label}</td>
                            <td>{$form.vat_rate.html} &nbsp; (or) &nbsp;	<!--sql select box-->
								{$form.vat_rate_member_rate_sql_id.html}&nbsp;
							
						
    	
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the VAT Rate(%) or select a SQL which returns the VAT Rate.{/ts}
        		                </span>
                            </td>
                        </tr>
						      <tr id="zero_rate">
                            <td class="label">{$form.zero_rate.label}</td>
                            <td>{$form.zero_rate.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the Zero Rate{/ts}
        		                </span>
                            </td>
                        </tr>
                        <tr id="vat_weighting_row">
                            <td class="label">{$form.vat_weighting.label}</td>
                            <td>{$form.vat_weighting.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the VAT Weighting(%). i.e. VAT applied to 50% or 100% of the CiviCRM Contribution Amount{/ts}
        		                </span>
                            </td>
                        </tr>
                        
                        
                        
                        <tr id="nominal_row">
                            <td class="label">{$form.excempt.label}</td>
                            <td>{$form.excempt.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the Excempt. i.e. Excempt Value{/ts}
        		                </span>
                            </td>
                        </tr>
                        
                       
                        
                        
                        
                        
                        </table>
                        
                    </td>        
                </tr>
                
                <tr>
                
                 <tr>
                 
                 
                     
                        
                        
                        
                        <!--sri code ends here on 9th May 2013-->
                 

			</table>
		</td>
     </tr>
</table>
<div class="crm-submit-buttons">
  {include file="CRM/common/formButtons.tpl" location="top"}
</div>
</div>

{else if $action eq '8'}
    <h3>Delete invoice settings for {$contribution_type}</h3>
		<div class="crm-participant-form-block-delete messages status">
        <div class="crm-content">
            <div class="icon inform-icon"></div> &nbsp;
            {ts}WARNING: This operation cannot be undone.{/ts} {ts}Do you want to continue?{/ts}
        </div>
    </div>
    {assign var=id value=$id}
    <div class="crm-submit-buttons">
        <a class="button" href="{crmURL p='civicrm/admin/invoice/vatcodesetting/add' q="action=force_delete&id=$id&reset=1"}"><span><div class="icon delete-icon"></div>{ts}Delete{/ts}</span></a>        
        <a class="button" href="{crmURL p='civicrm/admin/invoice/vatcode/setting' q="reset=1"}"><span>{ts}Cancel{/ts}</span></a>
    </div>    
    </div>
{/if}


{literal}
<script type="text/javascript">
cj(document).ready( function() {
        //alert(cj("#vat_rate").val());


cj("#vat_rate").on("blur", function(){ 
if(cj("#vat_rate").val() != '')
  {
    cj("#vat_rate_member_rate_sql_id").attr('disabled','disabled');
  }
else
  {
    cj("#vat_rate_member_rate_sql_id").prop('disabled', false);
  }

 });


 //and
 
 if(cj("#vat_rate").val() != '')
  {
    cj("#vat_rate_member_rate_sql_id").attr('disabled','disabled');
  }
 //for select box


cj("#vat_rate_member_rate_sql_id").on("blur", function(){ 
if(cj("#vat_rate_member_rate_sql_id").val() != '')
  {
    cj("#vat_rate").attr('disabled','disabled');
  }
else
  {
    cj("#vat_rate").prop('disabled', false);
  }

 });
 
 //and
 if(cj("#vat_rate_member_rate_sql_id").val() != '')
  {
    cj("#vat_rate").attr('disabled','disabled');
  }

});




cj('#_qf_VatCodeSetting_next-top').click(function() {

  //alert(cj('#vat_rate').val());
 if(cj("#vat_rate").val() == '' && cj("#vat_rate_member_rate_sql_id").val() == '') {
        alert('Please Enter Vat Rate or Select Vat Rate SQL ');
        return false;    
    }
});



</script>
{/literal}
