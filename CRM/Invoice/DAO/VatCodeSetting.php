<?php
/*
+--------------------------------------------------------------------+
| CiviCRM version 4.1                                                |
+--------------------------------------------------------------------+
| Copyright CiviCRM LLC (c) 2004-2011                                |
+--------------------------------------------------------------------+
| This file is a part of CiviCRM.                                    |
|                                                                    |
| CiviCRM is free software; you can copy, modify, and distribute it  |
| under the terms of the GNU Affero General Public License           |
| Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
|                                                                    |
| CiviCRM is distributed in the hope that it will be useful, but     |
| WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
| See the GNU Affero General Public License for more details.        |
|                                                                    |
| You should have received a copy of the GNU Affero General Public   |
| License and the CiviCRM Licensing Exception along                  |
| with this program; if not, contact CiviCRM LLC                     |
| at info[AT]civicrm[DOT]org. If you have questions about the        |
| GNU Affero General Public License or the licensing of CiviCRM,     |
| see the CiviCRM license FAQ at http://civicrm.org/licensing        |
+--------------------------------------------------------------------+
*/
/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2011
 * $Id$
 *
 */
require_once 'CRM/Core/DAO.php';
require_once 'CRM/Utils/Type.php';

class CRM_Invoice_DAO_VatCodeSetting extends CRM_Core_DAO
{
    /**
     * static instance to hold the table name
     *
     * @var string
     * @static
     */	
    static $_tableName = CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE;
    /**
     * static instance to hold the field values
     *
     * @var array
     * @static
     */
    static $_fields = null;
    /**
     * static instance to hold the FK relationships
     *
     * @var string
     * @static
     */
    static $_links = null;
    /**
     * static instance to hold the values that can
     * be imported / apu
     *
     * @var array
     * @static
     */
    static $_import = null;
    /**
     * static instance to hold the values that can
     * be exported / apu
     *
     * @var array
     * @static
     */
    static $_export = null;
    /**
     * static value to see if we should log any modifications to
     * this table in the civicrm_log table
     *
     * @var boolean
     * @static
     */
    static $_log = false;
    /**
     * Unique ID
     *
     * @var int unsigned
     */
    public $id;
    
    /**
     * vatcode Codes
     *
     * @var int unsigned
     */
    public $vat_code;
    
    /**
     * Vat Rate
     *
     * @var varchar
     */
    public $vat_rate;
    /**
     * Zero Rate
     *
     * @var varchar
     */
    public $zero_rate;
    /**
     * VAT Weighting %
     *
     * @var varchar
     */
    public $vat_weighting;
    /**
     * VAT Excempt %
     *
     * @var varchar
     */
    public $excempt;
    /**
     * vat_rate_member_rate_sql_id from price set customization table		
     *
     * @var varchar
     */
	  public $vat_rate_member_rate_sql_id;
    
    
    function __construct()
    {
        parent::__construct();
    }
    /**
     * return foreign links
     *
     * @access public
     * @return array
     */
    function &links()
    {
        return self::$_links;
    }
    /**
     * returns all the column names of this table
     *
     * @access public
     * @return array
     */
    static function &fields()
    {
        if (!(self::$_fields)) {
            self::$_fields = array(
                'id' => array(
                    'name' => 'id',
                    'type' => CRM_Utils_Type::T_INT,
                    'required' => true,
                ) ,
                'vat_rate' => array(
                    'name' => 'vat_rate',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts('Vat Rate') ,
                    'size' => CRM_Utils_Type::HUGE,
                ) ,
                 'vat_code' => array(
                    'name' => 'vat_code',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts('Vat Code') ,
                    'size' => CRM_Utils_Type::HUGE,
                ) ,
                'zero_rate' => array(
                    'name' => 'zero_rate',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Zero Rate') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'vat_weighting' => array(
                    'name' => 'vat_weighting',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' vat Weighting') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'excempt' => array(
                    'name' => 'excempt',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Excempt') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
				'vat_rate_member_rate_sql_id' => array(
                    'name' => 'vat_rate_member_rate_sql_id',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' vat_rate_member_rate_sql_id') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'cost_centre' => array(
                    'name' => 'cost_centre',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Cost Centre') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
				 'department' => array(
                    'name' => 'department',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Department') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'calculate_vat' => array(
                    'name' => 'calculate_vat',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Calculate Vat?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'is_vat_inclusive' => array(
                    'name' => 'is_vat_inclusive',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Is VAT Inclusive?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'create_pdf_invoice' => array(
                    'name' => 'create_pdf_invoice',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Create PDF Invoice?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'attach_to_email' => array(
                    'name' => 'attach_to_email',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Attach Invoice to Email?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
            );
        }
        return self::$_fields;
    }
    /**
     * returns the names of this table
     *
     * @access public
     * @return string
     */
    static function getTableName()
    {
        return CRM_Core_DAO::getLocaleTableName(self::$_tableName);
    }
}
