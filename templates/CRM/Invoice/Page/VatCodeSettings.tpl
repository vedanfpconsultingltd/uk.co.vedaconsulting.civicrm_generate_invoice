	{*
 +--------------------------------------------------------------------+
 | CiviCRM version 3.3                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*}

<div class="crm-form-block">
<div class="help">
    <p>
    {ts}You can add/edit VAT Codes here.{/ts}
    </p>
</div>

<table class="form-layout-compressed">
    <tr>
      <td>
          <a class="button" href="{crmURL p="civicrm/admin/invoice/vatcodesetting/add" q="action=add&reset=1"}" accesskey='e'><span><div class="icon add-icon"></div>&nbsp;Add New</span></a>
      </td>
    </tr>
</table>
{if $invoiceSettingsCount eq '0'}
  <div class="messages status">
      <div class="icon inform-icon"></div>&nbsp;
      There are no VAT codes added.
      {ts 1=$crmURL} You can <a href='{crmURL p="civicrm/admin/invoice/vatcodesetting/add" q="action=add&reset=1"}'>add one</a>.{/ts}
  </div>
{else}
  <div>
  <table  style="border: 1px solid #A3A1A1;" class="sortable">
  <thead>
  <tr>
     <!-- <td style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Contribution Type{/ts}</td>
    <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Message Template ID{/ts}</td>-->
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Code{/ts}</td>
	 <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Rate(%){/ts}</td>
   <td style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Vat Rate SQL ID{/ts}</td>
	 <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Weighting(%){/ts}</td>
	 <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Zero Rate(%){/ts}</td>
	 <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Excempt(%){/ts}</td>
     <!--<td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Rate(%){/ts}</td>
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Weighting(%){/ts}</td>
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Is Amount Gross?{/ts}</td>
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Create PDF Invoice?{/ts}</td>-->

     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Actions{/ts}</td>
  </tr>
  </thead>
  <tbody>
   {foreach from=$invoiceSettingsArray item=row}
   {assign var=id value=$row.id}
   <tr>
      <!--<td width="10%">{$row.contribution_type_id}</td>-->
     
      <td width="10%" align="center">{$row.vat_code}</td>
	   <td width="10%" align="center">{$row.vat_rate}</td>
     <td width="10%" align="center">{$row.vat_rate_member_rate_sql_id}</td>
	   	   <td width="10%" align="center">{$row.vat_weighting}</td>
		   	   <td width="10%" align="center">{$row.zero_rate}</td>
			   	   <td width="10%" align="center">{$row.excempt}</td>
      <!--
      -->
      <td width="20%" align="center">
          <a href="{crmURL p="civicrm/admin/invoice/vatcodesetting/add" q="action=update&id=$id&reset=1"}">Edit</a>&nbsp;  
          <a href="{crmURL p="civicrm/admin/invoice/vatcodesetting/add" q="action=delete&id=$id&reset=1"}">Delete</a>
      </td>
    </tr>
   {/foreach}
   </tbody>
   </table> 
   </div>
{/if}
</div>
