{*
 +--------------------------------------------------------------------+
 | CiviCRM version 3.3                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*}

<div class="crm-form-block">
<div class="help">
    <p>
    {ts}You can add/edit VAT and Invoice settings for each contribution types here.{/ts}
    </p>
</div>

<table class="form-layout-compressed">
    <tr>
      <td>
          <a class="button" href="{crmURL p="civicrm/admin/invoice/setting/add" q="action=add&reset=1"}" accesskey='e'><span><div class="icon add-icon"></div>&nbsp;Add New</span></a>
      </td>
    </tr>
</table>
{if $invoiceSettingsCount eq '0'}
  <div class="messages status">
      <div class="icon inform-icon"></div>&nbsp;
      There are no VAT and Invoice settings added.
      {ts 1=$crmURL} You can <a href='{crmURL p="civicrm/admin/invoice/setting/add" q="action=add&reset=1"}'>add one</a>.{/ts}
  </div>
{else}
  <div>
  <table  style="border: 1px solid #A3A1A1;" class="sortable">
  <thead>
  <tr>
     <td style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Contribution Type{/ts}</td>
     <!--<td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Message Template ID{/ts}</td>-->
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Calculate VAT?{/ts}</td>
     <!--<td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Rate(%){/ts}</td>
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}VAT Weighting(%){/ts}</td>
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Is Amount Gross?{/ts}</td>-->
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Create PDF Invoice?{/ts}</td>
     <td align="center" style="background-color: #CDCDCD; color: #000000; font-weight: bold; border: none;">{ts}Actions{/ts}</td>
  </tr>
  </thead>
  <tbody>
   {foreach from=$invoiceSettingsArray item=row}
   {assign var=id value=$row.id}
   <tr>
      <td width="10%">{$row.financial_type_id}</td>
      <!--<td width="10%" align="center">{$row.msg_template_id}</td>-->
      <td width="10%" align="center">{if $row.calculate_vat eq '1'}Yes{else}No{/if}</td>
      <td width="10%" align="center">{if $row.create_pdf_invoice eq '1'}Yes{else}No{/if}</td>
      <!--<td width="10%" align="center">{if $row.vat_percentage neq 'NULL'}{$row.vat_percentage}{else}n/a{/if}</td>
      <td width="10%" align="center">{if $row.vat_weighting neq 'NULL'}{$row.vat_weighting}{else}n/a{/if}</td>
      <td width="10%" align="center">{if $row.display_vat_element neq '0'}{if $row.is_vat_inclusive neq '0'}Yes{else}No{/if}{else}n/a{/if}</td>-->
      <td width="20%" align="center">
          <a href="{crmURL p="civicrm/admin/invoice/setting/add" q="action=update&id=$id&reset=1"}">Edit</a>&nbsp;  
          <a href="{crmURL p="civicrm/admin/invoice/setting/add" q="action=delete&id=$id&reset=1"}">Delete</a>
      </td>
    </tr>
   {/foreach}
   </tbody>
   </table> 
   </div>
{/if}
</div>
