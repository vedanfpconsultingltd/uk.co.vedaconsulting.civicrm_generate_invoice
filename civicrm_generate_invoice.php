<?php
/*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.4                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2011                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2011
 * $Id$
 *
 */

define ('CIVICRM_INVOICE_SETTINGS_TABLE' , 'civicrm_invoice_setting');
define ('CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE' , 'civicrm_invoice_vatcode_setting');

/**
 * Implementation of hook_civicrm_install
 * To import custom fields and invoice settings table during installation
 */
function civicrm_generate_invoice_civicrm_install( ) {

	$civiGenInvoiceRoot = dirname( __FILE__ ) . DIRECTORY_SEPARATOR;
	$civiGenInvoiceXMLFile = $civiGenInvoiceRoot . DIRECTORY_SEPARATOR . 'CustomGroupData.xml';
	require_once 'CRM/Utils/Migrate/Import.php';    $import = new CRM_Utils_Migrate_Import( );
	$import->run( $civiGenInvoiceXMLFile );

	// Create the DB table to store the extension settings, message template id, etc
	$query = "CREATE TABLE IF NOT EXISTS `civicrm_invoice_extension_setting` (
						`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
						`msg_template_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						PRIMARY KEY (`id`)
						) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
	CRM_Core_DAO::executeQuery( $query );

	// Create the DB table to store the invoice settings
	$query = "CREATE TABLE IF NOT EXISTS `civicrm_invoice_setting` (
						`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
						`financial_type_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Contribution types for which the invoice needs to be generated',
						`msg_template_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						`display_vat_element` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						`vat_code` VARCHAR( 255 ) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT  'VAT Code',
						`nominal` VARCHAR( 255 ) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT  'Nominal',
						`cost_centre` VARCHAR( 255 ) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT  'Cost Centre',
						`department` VARCHAR( 255 ) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT  'Department',
						`is_vat_inclusive` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						`calculate_vat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						`create_pdf_invoice` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						`attach_to_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL ,
						PRIMARY KEY (`id`)
						) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
	CRM_Core_DAO::executeQuery( $query );

	//create the table to store the VatCode details
	$query = "CREATE TABLE IF NOT EXISTS `civicrm_invoice_vatcode_setting` (
						`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
						`vat_code` VARCHAR( 255 ) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT  'VAT Code',
						`vat_rate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Vat Rate (%)',
						`zero_rate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Zero Rate (%)',
						`vat_weighting` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Vat Weighting (%)',
						`excempt` VARCHAR( 255 ) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT  'Nominal',
						`vat_rate_member_rate_sql_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Vat Rate (%)',
						PRIMARY KEY (`id`)
						) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
	CRM_Core_DAO::executeQuery( $query );

	$query1 = "CREATE TABLE if NOT EXISTS `mtl_civicrm_members_rate_sql` (
              `id` int(10) unsigned NOT null AUTO_INCREMENT COMMENT 'Default MySQL primary key',
              `member_rate_title` VARCHAR(255) COMMENT 'Title',
              `member_rate_sql` TEXT   COMMENT 'custom SQL',
               PRIMARY KEY (`id`)
              ) default CHARSET=utf8 COLLATE=utf8_unicode_ci;";
  CRM_Core_DAO::executeQuery( $query1 );

	// Import invoice message template when the module is installed
	$config = CRM_Core_Config::singleton( );
	$file_url = dirname(__FILE__)."/invoice_html.html";
	if(is_file($file_url)) {
		$invoice_html = file_get_contents($file_url);

		require_once 'CRM/Core/DAO/MessageTemplate.php';
		$msgTemplateDao = new CRM_Core_DAO_MessageTemplate();
		$msgTemplateDao->msg_title = 'Invoice template';
		$msgTemplateDao->msg_subject = 'Invoice template';
		$msgTemplateDao->msg_text = $invoice_html;
		$msgTemplateDao->is_active = 1;
		$msgTemplateDao->is_default = 1;
		$msgTemplateDao->is_reserved = 0;
		$msgTemplateDao->save();

		$message_template_id = $msgTemplateDao->id;

		if ($message_template_id)	{
			$query = "REPLACE INTO civicrm_invoice_extension_setting(id , msg_template_id)
						 VALUES(1 , $message_template_id);";
			CRM_Core_DAO::executeQuery( $query );
		}
	}

	// Create 'Invoice' Payment Instrument
	$optionGroupParams = array('version'         => '3'
                            ,'name'            => 'payment_instrument');
	$optionGroup = civicrm_api('OptionGroup', 'Get', $optionGroupParams);

	$paymentParams = array('version'         => '3'
                         ,'option_group_id' => $optionGroup['id']
                         ,'name'            => 'Invoice'
                         ,'label'           => 'Invoice'
                         ,'description'     => 'Invoice');
	$paymentInstrument = civicrm_api('OptionValue', 'Create', $paymentParams);

  //rebuild the menu so our path is picked up
    require_once 'CRM/Core/Invoke.php';
    CRM_Core_Invoke::rebuildMenuAndCaches( );
}

function civicrm_generate_invoice_civicrm_config( &$config ) {

    $template =& CRM_Core_Smarty::singleton( );
    $civiGenInvoiceRoot = dirname( __FILE__ );
    $civiGenInvoiceDir = $civiGenInvoiceRoot . DIRECTORY_SEPARATOR . 'templates';
    if ( is_array( $template->template_dir ) ) {
        array_unshift( $template->template_dir, $civiGenInvoiceDir );
    } else {
        $template->template_dir = array( $civiGenInvoiceDir, $template->template_dir );
    }
    // also fix php include path
    $include_path = $civiGenInvoiceRoot . PATH_SEPARATOR . get_include_path( );
    set_include_path( $include_path );
}

function civicrm_generate_invoice_civicrm_xmlMenu( &$files ) {
    $files[] = dirname(__FILE__)."/xml/Menu/Invoice.xml";
}

/**
 * Implementation of Naivagation menu hook
 */
function civicrm_generate_invoice_civicrm_navigationMenu( &$params ) {
	foreach ($params as $menu_id => $menu ) {
		if ($menu['attributes']['name'] == 'Administer') {
			foreach ($menu['child'] as $child_id => $child ) {
				if ($child['attributes']['name'] == 'CiviContribute') {
					$key = max(array_keys($child['child']));
					$params[$menu_id]['child'][$child_id]['child'][$key]['attributes']['separator'] = 1;
//					$params[$menu_id]['child'][$child_id]['child'][] = array (
//																		'attributes' => array (
//																				'label'      => 'VAT Codes',
//																				'name'       => 'VAT Codes',
//																				'url'        => 'civicrm/admin/invoice/vatcode/setting?reset=1',
//																				'permission' => 'administer CiviCRM',
//																				'operator'   => null,
//																				'separator'  => 0,
//																				'parentID'   => $child_id,
//																				'navID'      => $key + 1,
//																				'active'     => 1
//																		),
//																		'child' => null
//																);
					$params[$menu_id]['child'][$child_id]['child'][] = array (
																		'attributes' => array (
//																													 'label'      => 'VAT and Invoice Settings',
//																													 'name'       => 'VAT_and_Invoice_Settings',
																													 'label'      => 'Invoice Settings',
																													 'name'       => 'Invoice_Settings',
																													 'url'        => 'civicrm/admin/invoice/setting?reset=1',
																													 'permission' => 'administer CiviCRM',
																													 'operator'   => null,
																													 'separator'  => 0,
																													 'parentID'   => $child_id,
																													 'navID'      => $key + 2,
																													 'active'     => 1
																														),
																		'child' => null
																		);
				}
			}
		}
	}
}


function civicrm_generate_invoice_civicrm_tokens( &$tokens ) {
    $tokens['contact']['contact.address_block'] = 'Address block';
}

function civicrm_generate_invoice_civicrm_tokenValues( &$values, &$contactIDs ) {
    require_once 'api/api.php';
    require_once 'CRM/Utils/Address.php';
    foreach($contactIDs as $id) {
		$contactParams = array ('q' =>'civicrm/ajax/rest', 'version' =>'3' , 'id'=> $id);
        $contactResult = civicrm_api("Contact" , "get" , $contactParams );
        $values[$id]['contact.address_block'] = nl2br(CRM_Utils_Address::format($contactResult['values'][0]));
    }
}

function civicrm_generate_invoice_civicrm_buildForm($formName, &$form) {
	require_once 'CRM/Core/Session.php';
	$session = CRM_Core_Session::singleton();
	$session->set('CreatedContributionId' , '');
	$session->set('ContactId' , '');
	$session->set('CreatedParticipantId' , '');

	// Pre-populate contribution form, if it is a credit note
	if ($formName == 'CRM_Contribute_Form_Contribution') {
		$original_contribution_id = CRM_Utils_Request::retrieve( 'oc', 'Integer', $this );
		$extra_action = CRM_Utils_Request::retrieve( 'eaction', 'String', $this );
		if (empty($original_contribution_id))	{
			$original_contribution_id = CRM_Utils_Array::value('oc', $_POST, '');
		}
		if (empty($extra_action))	{
			$extra_action = CRM_Utils_Array::value('eaction', $_POST, '');
		}

		if (!empty($original_contribution_id) AND $extra_action == 'create_credit_note') {
			// get the original contribution details
			require_once 'CRM/Contribute/DAO/Contribution.php';
			$contribdao = new CRM_Contribute_DAO_Contribution();
			$contribdao->id = $original_contribution_id;
			$contribdao->find(true);

			$defaults = array(
													'financial_type_id'     => $contribdao->financial_type_id,
													'total_amount'          => '-'.$contribdao->total_amount,
													'currency'              => $contribdao->currency,
													'source'                => $contribdao->source,
													'payment_instrument_id' => $contribdao->payment_instrument_id,
													'contribution_status_id'=> $contribdao->contribution_status_id,
											 );
			$form->freeze(array('currency'));
			$form->setDefaults( $defaults );

			$form->addElement('hidden', 'oc',  $original_contribution_id );
			$form->addElement('hidden', 'eaction', $extra_action );
		}
	}
}

function civicrm_generate_invoice_civicrm_validate( $formName, &$fields, &$files, &$form )
{
	if ($formName == 'CRM_Contribute_Form_Contribution') {
		if (isset($fields['eaction']) AND $fields['eaction'] == 'create_credit_note')	{
			if ($fields['total_amount'] >= 0) {
					$errors["total_amount"] = ts( "Total amount should be a negative value for Credit Note." );
			}
		}
	}
	return empty($errors) ? true : $errors;
}

function civicrm_generate_invoice_civicrm_pageRun( &$page ) {
	$action = CRM_Utils_Array::value('action', $_GET, '');
	if ( $page->getVar( '_name' ) == 'CRM_Contribute_Page_Tab') {
		if ($action == 'create-invoice' AND !empty($page->_id)) {
			civicrm_generate_invoice_civicrm_post_createInvoice($page->_id);
			CRM_Core_Session::setStatus(ts("Invoice created for the contribution. You can download the invoice under <b>Invoice Details</b> custom data."));
			CRM_Utils_System::redirect(CRM_Utils_System::refererPath());
		}
		require_once 'CRM/Contribute/DAO/Contribution.php';
		$contribdao = new CRM_Contribute_DAO_Contribution();
		$contribdao->id = $page->_id;
		$contribdao->find(true);

		require_once 'CRM/Invoice/DAO/Setting.php';
		$invoiceSettingsdao = new CRM_Invoice_DAO_Setting();
		$invoiceSettingsdao->financial_type_id = $contribdao->financial_type_id;
		if ($invoiceSettingsdao->find(true)) {
			$page->assign('enableInvoiceButton' , 1);
		}
	}
}

/**
 * Implementation of hook_civicrm_post
 * Function to update the credits for a contact, depending on the fee(s) paid
 */
function civicrm_generate_invoice_civicrm_post( $op, $objectName, $objectId, &$objectRef ) {
	if ($op == 'create' && $objectName == 'Contribution')	{
		// Set the invoice is not printed
		$sql = "REPLACE INTO civicrm_value_invoice_details SET entity_id = $objectId , invoice_printed = 0";
		$dao = CRM_Core_DAO::executeQuery( $sql );
	}
  if (($op == 'create' OR $op == 'edit') AND $objectName == 'Contribution' ) {

  	if (empty($objectId)) {
  		return;
  	}

		$config = CRM_Core_Config::singleton( );
		$seperator = $config->fieldSeparator;

		require_once 'CRM/Invoice/DAO/Setting.php';
		$invoiceSettingsdao = new CRM_Invoice_DAO_Setting();
		$invoiceSettingsdao->financial_type_id = $objectRef->financial_type_id;

		if (!$invoiceSettingsdao->find(true)) {
				return;
		}

		require_once 'CRM/Core/Session.php';
		$session = CRM_Core_Session::singleton();
		$session->set('CreatedContributionId' , $objectId);

		$sqlParams = array( 1 => array( $objectId                 , 'Integer' ),
												2 => array( sprintf("%06d",$objectId) , 'String'  ),
											 );

		$extra_sql = '';
		$extra_sql_number = '';
		$extra_sql_duplicate = '';
		if (isset($_POST['eaction']) AND $_POST['eaction'] == 'create_credit_note' AND isset($_POST['oc']) AND !empty($_POST['oc'])) {
				$original_contribution_id = $_POST['oc'];
				$original_invoice_no = civicrm_generate_invoice_civicrm_post_get_original_invoice_number($original_contribution_id);
				if ($original_invoice_no) {
						$sqlParams[3]           = array( sprintf("%06d", $original_invoice_no) , 'String'  );
						$extra_sql              = " , original_invoice_number";
						$extra_sql_number       = " , %3";
						$extra_sql_duplicate    = ", original_invoice_number = %3";
				}
		}

		$query = "
		INSERT INTO civicrm_value_invoice_details
			(entity_id , invoice_number {$extra_sql})
		VALUES
			( %1 , %2 {$extra_sql_number})
		ON DUPLICATE KEY UPDATE
		invoice_number = %2 {$extra_sql_duplicate}
		";

		CRM_Core_DAO::executeQuery( $query, $sqlParams );
  }
}

function civicrm_generate_invoice_civicrm_postProcess( $formName, &$form ) {
	require_once 'CRM/Core/Session.php';
	$session = CRM_Core_Session::singleton();
	$CreatedContributionId = $session->get('CreatedContributionId');

  // Create the invoice and update vat details
	if (!empty($CreatedContributionId)) {

    require_once 'CRM/Contribute/DAO/Contribution.php';
		$contribdao = new CRM_Contribute_DAO_Contribution();
		$contribdao->id = $CreatedContributionId;
		$contribdao->find(true);

		$invoiceSettingsdao = new CRM_Invoice_DAO_Setting();
		$invoiceSettingsdao->financial_type_id = $contribdao->financial_type_id;

		if (!$invoiceSettingsdao->find(true)) {
			return;
		}
		$resultArray = civicrm_generate_invoice_civicrm_post_createInvoice($CreatedContributionId);
	}
}

/**
 * Implementation of hook_civicrm_alterMailParams
 * To include the contributions in the confirmation email, inc. and exl. of VAT
 */
function civicrm_generate_invoice_civicrm_alterMailParams( &$params ) {
	require_once 'CRM/Core/Session.php';
	$session = CRM_Core_Session::singleton();
	$CreatedContributionId = $session->get('CreatedContributionId');

	if (!empty($CreatedContributionId)) {
		require_once 'CRM/Contribute/DAO/Contribution.php';
		$contribdao = new CRM_Contribute_DAO_Contribution();
		$contribdao->id = $CreatedContributionId;
		$contribdao->find(true);

		require_once 'CRM/Invoice/DAO/Setting.php';
		$invoiceSettingsdao = new CRM_Invoice_DAO_Setting();
		$invoiceSettingsdao->financial_type_id = $contribdao->financial_type_id;
		if (!$invoiceSettingsdao->find(true)) {
			return;
		}
		if ($invoiceSettingsdao->attach_to_email == 1) {
			$resultArray = civicrm_generate_invoice_civicrm_post_createInvoice($CreatedContributionId);
			## Commented out to test if attachments are causing the problem
			if (!empty($params['attachments'])) {
					$params['attachments'] = $params['attachments'] + array($resultArray[0] => $resultArray[1]);
			} else {
					$params['attachments'] = array($resultArray[0] => $resultArray[1]);
			}
		}
	}
}

function civicrm_generate_invoice_civicrm_post_createInvoice($contributionId,  $type = "internal" , $msg_template_id = null) {

	$config =& CRM_Core_Config::singleton( );
	require_once 'CRM/Contribute/DAO/Contribution.php';
	$contribdao = new CRM_Contribute_DAO_Contribution();
	$contribdao->id = $contributionId;
	$contribdao->find(true);

	$linesource = $contribdao->source;

  $membershipTypeName = $membershipLateFee = "";
	// Get the contribution currency
	if (!empty($contribdao->currency)) {
		require_once 'CRM/Financial/DAO/Currency.php';
		$currencydao = new CRM_Financial_DAO_Currency();
		$currencydao->name = $contribdao->currency;
		$currencydao->find(true);
		$currency = $currencydao->symbol;
	}	else {
		$currency = $config->defaultCurrencySymbol;
	}

	if ($contribdao->financial_type_id == 2){
		$memberPaymentDao = new CRM_Member_DAO_MembershipPayment();
		$memberPaymentDao->contribution_id = $contributionId;
		$memberPaymentDao->find(true);

		if ($memberPaymentDao->membership_id) {
			$entity_id = $memberPaymentDao->membership_id;
			$entity_table = 'civicrm_membership';

			require_once 'CRM/Member/DAO/Membership.php';
			$memberDao = new CRM_Member_DAO_Membership();
			$memberDao->id = $memberPaymentDao->membership_id;
			$memberDao->find(true);

			$linesource = $memberDao->source;

			require_once 'CRM/Member/DAO/MembershipType.php';
			$memberTypeDao = new CRM_Member_DAO_MembershipType();
			$memberTypeDao->id = $memberDao->membership_type_id;
			$memberTypeDao->find(true);

			if (defined('CIVICRM_MEMBERSHIP_TYPE_ADDITIONAL_FEE_TABLE')) {
				 $mem_additional_fee_sql = "SELECT * FROM ".CIVICRM_MEMBERSHIP_TYPE_ADDITIONAL_FEE_TABLE." WHERE membership_type_id=".$memberTypeDao->id;
				 $mem_additional_fee_dao = CRM_Core_DAO::executeQuery( $mem_additional_fee_sql);
				 $mem_additional_fee_dao->fetch();
				 $membershipTypeName = $memberTypeDao->name;
				 $membershipLateFee = $mem_additional_fee_dao->additional_amount;
			}
		}
	}

	require_once 'CRM/Contribute/PseudoConstant.php';
	$financialTypeName = CRM_Contribute_PseudoConstant::financialType($contribdao->financial_type_id);
	$contactId = $contribdao->contact_id;
	$invoiceHtml = civicrm_generate_invoice_civicrm_post_createInvoice_get_Invoice_Message_Template($contribdao->financial_type_id);
	$source = '';

	require_once 'CRM/Core/Smarty/resources/String.php';
	civicrm_smarty_register_string_resource();
	$smarty = CRM_Core_Smarty::singleton();

	// Replace the contact's name and address (no blank line in address)
	require_once 'api/api.php';
	if (!empty($contactId)) {
		$contactParams = array ('q' =>'civicrm/ajax/rest', 'version' =>'3' , 'id'=> $contactId);
		$contactResult = civicrm_api("Contact" , "get" , $contactParams );
	}

	if (!empty($contactResult['values'][$contactId]['display_name'])) {
		$smarty->assign('displayName' , $contactResult['values'][$contactId]['display_name']);
	}

	// Add the neccessary address items into an Array
	$fullAddress = array();
	foreach (array('street_address', 'supplemental_address_1', 'supplemental_address_2', 'city', 'state_province_name', 'postal_code') as $addressItem) {
		if (!empty($contactResult['values'][$contactId][$addressItem])) {
			$fullAddress[] = $contactResult['values'][$contactId][$addressItem];
		}
	}

	$smarty->assign('fullAddressBlock' , @implode('<br />' , $fullAddress));
	$total_amount = $contribdao->total_amount;

	$feeLabel = $financialTypeName;
	$ContributionLineTableHeadItemsHtml = '';
	$ContributionLineItemsHtml = '';
	$ContributionSubTotalHtml = '';
	$ContributionTotalAmountHtml = '';
	$ContributionPaymentLaterHtml = '';
	$ContributionVatAmountHtml = '';
	$VatTotal = '';
	$SubTotal = '';
	$TotalAmount = '';

	// Retrieve priceset if for the contribution or event page
	$priceSetSql = "SELECT * FROM civicrm_line_item WHERE entity_id = ".$contributionId;
	$priceSetDao = CRM_Core_DAO::executeQuery( $priceSetSql );

	// Check if the contribution is from price set
	// If yes, populate all lines items in the invoice
	$temp = 1;
	while ($priceSetDao->fetch())
	{

		require_once 'CRM/Invoice/DAO/Setting.php';
		$invoiceSettingsdao = new CRM_Invoice_DAO_Setting();
		$invoiceSettingsdao->financial_type_id = $priceSetDao->financial_type_id;

		//processing only if we got invoice Settings for particular financial types
		if ($invoiceSettingsdao->find(true)) {
			if (!empty($invoiceSettingsdao->vat_code ))	{
				// Retrieve priceset if for the contribution or event page
				require_once 'CRM/Invoice/DAO/VatCodeSetting.php';
				$VatCodeSettingDao = new CRM_Invoice_DAO_VatCodeSetting();
				$VatCodeSettingDao->vat_code = $invoiceSettingsdao->vat_code ;
				$VatCodeSettingDao->find(true);
			}

			//getting new Vat Rate from Sql and assigning to vatcode settings vat_rate field.
			if($invoiceSettingsdao->calculate_vat == 1 && $VatCodeSettingDao->vat_rate == '')	{
					$sql = "SELECT member_rate_sql,vat_code,vat_rate from mtl_civicrm_members_rate_sql INNER JOIN civicrm_invoice_vatcode_setting ON mtl_civicrm_members_rate_sql.id=civicrm_invoice_vatcode_setting.vat_rate_member_rate_sql_id;";

					$dao = CRM_Core_DAO::executeQuery( $sql);
					$dao ->fetch();

					$newVatSql = $dao->member_rate_sql;

					$newVatSqldao = CRM_Core_DAO::executeQuery( $newVatSql);
					$newVatSqldao ->fetch();
					$VatCodeSettingDao->vat_rate = $newVatSqldao->id;//temporary assigned id..but needs to be replaced by tax field(value)
			}
		}

		if ($invoiceSettingsdao->create_pdf_invoice <> 1) {
			return;
		}	else {
			if(empty($VatCodeSettingDao->vat_code))
			{
				require_once 'CRM/Invoice/DAO/VatCodeSetting.php';
				$VatCodeSettingDao = new CRM_Invoice_DAO_VatCodeSetting();
				$VatCodeSettingDao->vat_code = $invoiceSettingsdao->vat_code ;
				if(!$VatCodeSettingDao->find(true)) {
					$VatCodeSettingDao->vat_rate = '';
					$VatCodeSettingDao->vat_weighting = '';
					$VatCodeSettingDao->zero_rate = '';
					$VatCodeSettingDao->excempt = '';
				}
			}
		}

		$SubTotal = 0;
		$incTotalAmount = 0;

		if($temp == 1) {
			if ($invoiceSettingsdao->display_vat_element == 1) {
					$ContributionLineTableHeadItemsHtml .= '<!--<tr bgcolor="#eee">
																											<th bgcolor="#B7B7B5" colspan="5" align="left"><b>'.$source.'</b></th>
																							</tr>-->
																									 <tr bgcolor="#eee">
																							<th bgcolor="#D6D6D6" align="left" width="45%">Description</th>
																							<th bgcolor="#D6D6D6" align="left" width="10%">Quantity</th>
																							<th bgcolor="#D6D6D6" align="right" width="15%">Unit Price</th>
																							<th bgcolor="#D6D6D6" align="right" width="15%">VAT</th>
																							<th bgcolor="#D6D6D6" align="right" width="15%">Amount</th>
																							 </tr>';
		 } else {
					$ContributionLineTableHeadItemsHtml .= '<!--<tr bgcolor="#eee">
																											<th bgcolor="#B7B7B5" colspan="4" align="left"><b>'.$source.'</b></th>
																							</tr>-->
																									<tr bgcolor="#eee">
																							<th bgcolor="#D6D6D6" align="left" width="60%">Description</th>
																							<th bgcolor="#D6D6D6" align="left" width="10%">Quantity</th>
																							<th bgcolor="#D6D6D6" align="right" width="15%">Unit Price</th>
																							<th bgcolor="#D6D6D6" align="right" width="15%">Amount</th>
																						 </tr>';
			}
		}
		$temp++;

		// while ($selectDao->fetch()) {
		$unitPrice = $priceSetDao->unit_price;
		$lineTotal = $priceSetDao->line_total;

        // Vat elements
        if ($invoiceSettingsdao->calculate_vat == 1)
        {
            if ($invoiceSettingsdao->is_vat_inclusive == 1)
            {
                $lineTotalAmount = $lineTotal;
                $lineVatAmount = civicrm_generate_invoice_civicrm_post_createInvoice_calculate_vat_from_gross($lineTotal , $VatCodeSettingDao->vat_rate , $VatCodeSettingDao->vat_weighting);
                $lineSubTotal = $lineTotalAmount - $lineVatAmount;
            }
            else
            {
                $lineSubTotal =  $lineTotal;
                $lineVatAmount = civicrm_generate_invoice_civicrm_post_createInvoice_calculate_vat_from_net($lineTotal , $VatCodeSettingDao->vat_rate , $VatCodeSettingDao->vat_weighting);
                $lineTotalAmount = $lineSubTotal + $lineVatAmount;

                $lineSubTotal =  $lineTotal;
                $lineVatAmount = civicrm_generate_invoice_civicrm_post_createInvoice_calculate_vat_from_net($lineTotal , $VatCodeSettingDao->vat_rate , $VatCodeSettingDao->vat_weighting);
				$lineTotal = $lineSubTotal + $lineVatAmount;
            }

            //$lineVatAmount = $lineTotal * 0.2;
            $VatTotal += $lineVatAmount;
            $SubTotal += $lineSubTotal;
            $TotalAmount += $lineTotalAmount;
            if ($invoiceSettingsdao->display_vat_element == 1)
            {
                $lineVatElement = '<td {$valueStyle} align="right">'.$currency.sprintf("%01.2f", abs($lineVatAmount)).'</td>';
                $colspan = 4;
            }
            else
            {
                $lineVatElement = '';
                $colspan = 3;
            }

                  $colspan = 4;
        }
        else
        {
            $SubTotal += $lineTotal;
            $lineVatElement = '';
            $colspan = 3;
            $TotalAmount += $lineTotal;
        }

        $priceFieldSql = "select * from civicrm_price_field where id = ".$priceSetDao->price_field_id;
        $priceFieldDao = CRM_Core_DAO::executeQuery( $priceFieldSql );
        $priceFieldDao->fetch();

        // $priceOptionLabel = $priceFieldDao->label;
         // $itemQuantity = $priceSetDao->qty;

		$priceOptionLabel = $priceSetDao->label . ':';
        $itemQuantity = $priceSetDao->qty;

        if (empty($linesource)) {
        	$linesource = $priceOptionLabel;
        }

	    //$ContributionLineItemsHtml .= '<div class="maindiv">
			//									<div class="halfdiv">'.$priceOptionLabel.'</div>
			//									<div class="halfdivalignright">'.$currency.sprintf("%01.2f", abs($lineTotal)).'</div>
			//								</div> ';

			$ContributionLineItemsHtml .= '<tr>
											  <td {$labelStyle} align="left">'.$linesource.'</td>
											  <td {$valueStyle} align="left">'.$itemQuantity.'</td>
											  <td {$valueStyle} align="right">'.$currency.sprintf("%01.2f", abs($unitPrice)).'</td>
											'.$lineVatElement.'
											<td {$valueStyle} align="right">'.$currency.sprintf("%01.2f", abs($lineTotal)).'</td>
											</tr>';

			//<td class="borderedcell" align="right">'.sprintf("%01.2f", $excVatUnitPrice).'</td>



        $ContributionSubTotalHtml = '<tr>
										<td {$labelStyle} align="left" colspan="'.$colspan.'"><b>Subtotal</b></td>
										<td {$valueStyle} align="right">'.$currency.sprintf("%01.2f", abs($SubTotal)).'</td>
									</tr>';

        if ($invoiceSettingsdao->display_vat_element == 1)
        {
            $ContributionVatAmountHtml = '<tr>
                          					<td {$labelStyle} align="left" colspan="'.$colspan.'"><b>VAT</b></td>
                          					<td {$valueStyle} align="right">'.$currency.sprintf("%01.2f", abs($VatTotal)).'</td>
                          			    </tr>';
        }

            //$ContributionTotalAmountHtml = '<div class="maindiv">
						//						<div class="halfdiv"><b>Total</b></div>
						//						<div class="halfdivalignright">'.$currency.sprintf("%01.2f", abs($TotalAmount)).'</div>
						//					</div> ';

						$ContributionTotalAmountHtml = '<tr>
                              					<td {$labelStyle} align="left" colspan="'.$colspan.'"><b>Total</b></td>
                              					<td {$valueStyle} align="right">'.$currency.sprintf("%01.2f", abs($TotalAmount)).'</td>
                          			       </tr>';

    }


    if ($invoiceSettingsdao->create_pdf_invoice == 1)
    {

		 //echo  '<br/> ContributionLineTableHeadItemsHtml:';print_r($ContributionLineTableHeadItemsHtml );
		 //echo  '<br/> ContributionLineItemsHtml:';print_r($ContributionLineItemsHtml );
		 //echo  '<br/> ContributionVatAmountHtml:';print_r($ContributionVatAmountHtml );
		 //echo  '<br/> ContributionSubTotalHtml:';print_r($ContributionSubTotalHtml );

		 // die;
        // Assign the contribution line Table Head items in the invoice content
        $smarty->assign('contributionLineTableHeadItemsHtml' , $ContributionLineTableHeadItemsHtml);
        // Assign the contribution line items in the invoice content
        $smarty->assign('contributionLineItems' , $ContributionLineItemsHtml);

        // Assign the contribution sub total in the invoice content
        $smarty->assign('contributionSubTotal' , $ContributionSubTotalHtml);

        // Assign the contribution VAT amount in the invoice content
        $smarty->assign('contributionVatAmountHtml' , $ContributionVatAmountHtml);

        // Assign the contribution total amount in the invoice content
        $smarty->assign('contributionTotalAmount' , $ContributionTotalAmountHtml);

        // Assign the contribution total amount in the invoice content
        //  $smarty->assign('displayAllInformationHtml' , $DisplayAllInformationHtml);



        // Display bank or pay later instructions, if pay later is ckecked or contribution status is pending (if offline)
        $smarty->assign('isPayLater' , $contribdao->is_pay_later);
        $smarty->assign('contributionStatusId' , $contribdao->contribution_status_id);

        //inserting values into custom field table civicrm_value_invoice_details.

        //$invoiceCustomSql = "UPDATE civicrm_value_invoice_details SET vat = abs($VatTotal) , vat_percentage = $VatCodeSettingDao->vat_rate , vat_weighting = $VatCodeSettingDao->vat_weighting, vat_code = $invoiceSettingsdao->vat_code, nominal = $invoiceSettingsdao->nominal , cost_centre = $invoiceSettingsdao->cost_centre, department = $invoiceSettingsdao->department, zero_rate = $VatCodeSettingDao->zero_rate, excempt = $VatCodeSettingDao->excempt WHERE entity_id = $contributionId";

        //var_dump($invoiceCustomSql);die;
        $invoiceCustomSql = "UPDATE civicrm_value_invoice_details SET vat = %2 , vat_percentage = %3 , vat_weighting = %4, vat_code = %5, nominal = %6, cost_centre = %7, department = %8, zero_rate = %9, excempt = %10 WHERE entity_id = %1";
        $invoiceCustomParams  = array(
                            1 => array( $contributionId , 'Integer') ,
                            2 => array( abs($VatTotal) , 'String') ,
        					3 => array( (string)$VatCodeSettingDao->vat_rate , 'String') ,
                            4 => array( (string)$VatCodeSettingDao->vat_weighting , 'String') ,
        					5 => array( (string)$invoiceSettingsdao->vat_code , 'String') ,
        					6 => array( (string)$invoiceSettingsdao->nominal , 'String') ,
                            7 => array( (string)$invoiceSettingsdao->cost_centre , 'String') ,
        					8 => array( (string)$invoiceSettingsdao->department , 'String') ,
                            9 => array( (string)$VatCodeSettingDao->zero_rate , 'String') ,
        				   10 => array( (string)$VatCodeSettingDao->excempt , 'String') ,
                           );
        CRM_Core_DAO::executeQuery( $invoiceCustomSql, $invoiceCustomParams );
        // print_r($invoiceCustomParams );die;

        $invoice_number = '';

        // add contribution id as invoice number
        // if invoice number is not populated in custom data field
        $invoiceCheckSql = "SELECT invoice_number , original_invoice_number FROM civicrm_value_invoice_details WHERE entity_id = ".$contributionId;
        $invoiceCheckDao = CRM_Core_DAO::executeQuery( $invoiceCheckSql );
        $invoiceCheckDao->fetch();
        if (empty($invoiceCheckDao->invoice_number)) {
        	$invoice_number = sprintf("%06d",$contributionId);
        	$sqlParams = array( 1 => array( $contributionId                 , 'Integer' ),
														2 => array( $invoice_number , 'String'  ),
											 	);
					$query = "UPDATE civicrm_value_invoice_details SET invoice_number = %2 WHERE entity_id = %1";
					CRM_Core_DAO::executeQuery( $query, $sqlParams );
        }

        //fetching custom field table civicrm_value_invoice_details values
        $ContribSql = "SELECT invoice_number , original_invoice_number ,vat_percentage, vat_weighting, zero_rate , excempt FROM civicrm_value_invoice_details WHERE entity_id = ".$contributionId;
        $ContribDao = CRM_Core_DAO::executeQuery( $ContribSql );

        if ($ContribDao->fetch())
        {
            $invoice_number = $ContribDao->invoice_number;
            $original_invoice_number = $ContribDao->original_invoice_number;

            // 'Credit Note' if negative contribution
            if ( $total_amount < 0)
            {
              $invoice_title = 'Credit Note';
            }
            else
            {
							$invoice_title = 'Invoice';
            }

            $smarty->assign('invoiceTitle' , $invoice_title);
            $smarty->assign('zero_rate' , $ContribDao->zero_rate);
            $smarty->assign('excempt' , $ContribDao->excempt);
            $smarty->assign('vat_percentage' , $ContribDao->vat_percentage);
            $smarty->assign('vat_weighting' , $ContribDao->vat_weighting);
        }
        // Assign the invoice number
        $smarty->assign('invoiceNumber' , $invoice_number);
        $smarty->assign('invoiceDate' , date('dS F Y'));
        $smarty->assign('year' , date('Y'));

        $smarty->assign('TotalAmount' , $currency.$TotalAmount);

        $smarty->assign('membershipName' , $membershipTypeName);

        $smarty->assign('LatePaymentFees' , $currency.$membershipLateFee);

        // Assign the original invoice number, if this is a credit note
        if (!empty($original_invoice_number))
        {
            $smarty->assign('originalInvoiceNumber' , $original_invoice_number);
        }

        //$invoiceHtml = $smarty->fetch("string:{$invoiceHtml}");
        // var_dump($invoiceHtml);die;

        //echo $invoiceHtml;die;

        $contact = $contactResult['values'];

        $contact_params = array(
                                'version' => 3,
                                'sequential' => 1,
                                'contact_id' => $contactId,
                               );

        require_once("CRM/Mailing/BAO/Mailing.php");
        $mailing = new CRM_Mailing_BAO_Mailing;
        $mailing->body_html = $invoiceHtml;
        $tokens = $mailing->getTokens();


        require_once("CRM/Utils/Token.php");
        $invoiceHtml    = CRM_Utils_Token::replaceDomainTokens($invoiceHtml,    $domain, true, $tokens['html']);

        if ($contactId)
        {
            $invoiceHtml    = CRM_Utils_Token::replaceContactTokens($invoiceHtml, $contact, false, $tokens['html']);
            //$invoiceHtml    = CRM_Utils_Token::replaceContactTokens($invoiceHtml, $contact, false, $tokens['text']);
        }

        $invoiceHtml = $smarty->fetch("string:{$invoiceHtml}");

        $fileName     = "{$invoice_number}.pdf";
        $csv_path = $config->customFileUploadDir;
        $filePathName   = "{$csv_path}{$fileName}";
        require_once 'CRM/Utils/PDF/Utils.php';
        $pdfContent = CRM_Utils_PDF_Utils::html2pdf($invoiceHtml , $fileName , true , null );

        $handle = fopen($filePathName, 'w');
        file_put_contents($filePathName, $pdfContent);
        fclose($handle);

        $attach = array('fullPath'=>$filePathName,
                      'mime_type'=>'pdf',
                      'cleanName'=>$fileName);

        $upload_date = date('Y-m-d H:i:s');

        require_once 'CRM/Core/DAO/File.php';
        $fileDao = new CRM_Core_DAO_File();
        $fileDao->mime_type = 'application/pdf';
        $fileDao->uri = $fileName;
        $fileDao->upload_date = $upload_date;
        $fileDao->save();
        $fileId = $fileDao->id;

        require_once 'CRM/Core/DAO/EntityFile.php';
        $efileDao = new CRM_Core_DAO_EntityFile();
        $efileDao->entity_id = $contributionId;
        $efileDao->entity_table = 'civicrm_value_invoice_details';
        $efileDao->file_id = $fileId;
        $efileDao->save();

        $invoiceCustomSql = "UPDATE civicrm_value_invoice_details SET invoice = %2 WHERE entity_id = %1";
        $invoiceCustomParams  = array(
                            1 => array( $contributionId , 'Integer') ,
                            2 => array( $fileId , 'Integer') ,
                           );
        CRM_Core_DAO::executeQuery( $invoiceCustomSql, $invoiceCustomParams );
    }

    if ($type == "external")
	{
		return $invoiceHtml;
	}

    return array($fileName , $attach);
}

/**
 * Function to get the Invoice Message template html
 */
function civicrm_generate_invoice_civicrm_post_createInvoice_get_Invoice_Message_Template($financial_type_id) {

    require_once 'CRM/Invoice/DAO/Setting.php';
    $dao = new CRM_Invoice_DAO_Setting();
    $dao->financial_type_id = $financial_type_id;
    //$dao->id = 1;
    $dao->find(true);

    require_once 'CRM/Core/DAO/MessageTemplate.php';
    $msgTemplateDao = new CRM_Core_DAO_MessageTemplate();
    $msgTemplateDao->id = $dao->msg_template_id;
    $msgTemplateDao->find(true);

    return $msgTemplateDao->msg_text;
}


/* g is the gross value of the contribution
 * v is the vat rate as a percentage integer
 * r is the range to apply the vat rate to. 100 represents vat on full amount
 * The value returned will be vat for the gross amount
 */
function civicrm_generate_invoice_civicrm_post_createInvoice_calculate_vat_from_gross($g, $vat, $r = 100) {
    $vatrate = $vat/100;
    $range = $r/100;
    //Calculates the net value from gross where the amount of VAT is not applied to the full net amount
    //The default is set calculate vat from the full gross amount (100%)
    $retval = ($range*$g) - (($range*$g)/(1 + $vatrate));
    return $retval;

}

/* net is the net value of the contribution
 * v is the vat rate as a percentage integer
 * r is the range to apply the vat rate to. 100 represents vat on full amount
 * The value returned will be vat for the net amount
 */
function civicrm_generate_invoice_civicrm_post_createInvoice_calculate_vat_from_net($net, $vat, $r = 100) {
    $vatrate = $vat/100;
    $range = $r/100;
    //Calculates the net value from gross where the amount of VAT is not applied to the full net amount
    //The default is set calculate vat from the full gross amount (100%)
    $retval = $net * $vatrate * $range;
    return $retval;

}

function civicrm_generate_invoice_civicrm_get_invoice_message_template()
{
    $query = "SELECT * FROM civicrm_invoice_extension_setting WHERE id = 1";
    $dao = CRM_Core_DAO::executeQuery( $query );
    if ($dao->fetch())
    {
        return $dao->msg_template_id;
    }
    return NULL;
}

function civicrm_generate_invoice_civicrm_post_get_original_invoice_number($contribution_id)
{
    if (empty($contribution_id))
    {
        return;
    }
    $ContribSql = "SELECT invoice_number as invoice_number FROM civicrm_value_invoice_details WHERE entity_id = ".$contribution_id;
    $ContribDao = CRM_Core_DAO::executeQuery( $ContribSql );
    if ($ContribDao->fetch()) {
        $invoice_number = $ContribDao->invoice_number;
        return $invoice_number;
    }
}

/**
 * Implentation Search column Hook to manipulate the Contribution Search Results
 */
function civicrm_generate_invoice_civicrm_searchColumns( $objectName, &$headers, &$rows, &$selector ) {
  // Add 'Create Credit Note' Column to contribution columns
  if($objectName == "contribution"){
		/*$headerParams = array(
				'name'      => ts(''),
		);*/
		//array_unshift($headers, $headerParams);
		array_splice($headers, 7, 0, array('7' => array('name' => ts('Credit Note'), 'sort' => '', 'direction' => '')));
	  foreach( $rows as $k => $row ){
			$contribution_id = $row['contribution_id'];
			$invoice_number = '';
			$original_invoice_number = '';
			if ($contribution_id)
			{
					$ContribSql = "SELECT invoice_number, original_invoice_number FROM civicrm_value_invoice_details WHERE entity_id = ".$contribution_id;
					$ContribDao = CRM_Core_DAO::executeQuery( $ContribSql );
					if ($ContribDao->fetch()) {
							$invoice_number = $ContribDao->invoice_number;
							$original_invoice_number = $ContribDao->original_invoice_number;
					}

					$rows[$k]['invoice_number'] = $invoice_number;
					$rows[$k]['original_invoice_number'] = $original_invoice_number;
			}
		}
  }
}//end function
