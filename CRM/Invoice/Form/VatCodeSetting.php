<?php

/*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.1                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2010
 * $Id$
 *
 */
require_once 'CRM/Core/Form.php';
require_once 'CRM/Core/Session.php';
require_once 'CRM/Contribute/PseudoConstant.php';

/**
 * This class provides the functionality for invoice settings
 */

class CRM_Invoice_Form_VatCodeSetting extends CRM_Core_Form {

    /**
     * build all the data structures needed to build the form
     *
     * @return void
     * @access public
     */
	function preProcess()
  {	
        parent::preProcess( );
		    
	}
	
    /**
     * Build the form
     *
     * @access public
     * @return void
     */
    function buildQuickForm( ) {

        $action = CRM_Utils_Array::value('action', $_REQUEST, '');
		
        if (empty($action))
            $action = CRM_Utils_Array::value('action', $_POST, '');
        
        $id = CRM_Utils_Request::retrieve( 'id', 'Integer', $this );
        if (empty($id))
            $id = CRM_Utils_Array::value('id', $_POST, '');

        CRM_Utils_System::setTitle( 'VAT and Invoice Settings' );

        $sql_params  = array( 1 => array( $id , 'Integer' ),);

        if ($action == 'update') {
        
            CRM_Utils_System::setTitle( 'Edit VAT and Invoice settings' );
        
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." WHERE id = %1";
            $dao = CRM_Core_DAO::executeQuery($sql , $sql_params);
            if($dao->fetch()) {
                        $defaults = array(
						 'vat_code'        	    		 => $dao->vat_code ,
						 'vat_rate'        	    		 => $dao->vat_rate ,
						 //'zero_rate'        			 => $dao->zero_rate ,
                         'vat_weighting'        		 => $dao->vat_weighting ,
						 'excempt'        	   			 => $dao->excempt , 
						 //'vat_rate_member_rate_sql_id'   => $dao->vat_rate_member_rate_sql_id ,						 
                          );
            }
            $this->setDefaults( $defaults );
            
        } elseif ($action == 'add') {
        
            CRM_Utils_System::setTitle( 'Add Vat Code' );
            
        } elseif ($action == 'delete') {
        
            $this->assign('id', $id );
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." WHERE id = %1";
            $dao = CRM_Core_DAO::executeQuery($sql , $sql_params);
            $dao->fetch();
            //$this->assign('contribution_type', CRM_Contribute_PseudoConstant::contributionType($dao->contribution_type_id));
            CRM_Utils_System::setTitle( 'Delete VAT and Invoice settings' );
            
        } elseif ($action == 'force_delete') {
        
            $sql = "DELETE FROM ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." WHERE id = %1";
    		CRM_Core_DAO::executeQuery($sql , $sql_params);
            
            $session = CRM_Core_Session::singleton( );
            $status = ts('Invoice setting deleted');
            CRM_Core_Session::setStatus($status);
            CRM_Utils_System::redirect(CRM_Utils_System::url( 'civicrm/admin/invoice/vatcode/setting', 'reset=1'));
            CRM_Utils_System::civiExit();
        }

        //$this->add('select', 'contribution_type_id', ts('Contribution Type'), array('- select -') + CRM_Contribute_PseudoConstant::contributionType(), TRUE);
        //$this->add('select', 'contribution_type_id', ts('Contribution Type'), array(''=>ts( '- select -' )) + CRM_Contribute_PseudoConstant::contributionType( ), true );
		$this->addElement( 'text', 'vat_code', ts('VAT Code '), array('size' => 15, 'maxlength' => 15), false ); 
		$this->add( 'text', 'vat_rate', ts('VAT Rate (%)'), array('size' => 5, 'maxlength' => 5), false );     
		//$this->add( 'text', 'zero_rate', ts('Zero Rate (%)'), array('size' => 5, 'maxlength' => 5), false );
      
    
        $this->add( 'text', 'vat_weighting', ts('VAT Weighting (%)'), array('size' => 5, 'maxlength' => 6), false );

		
		//memberes rate sql assigning starts here
		
		
		 //$sqlArray = CRM_MembersRate_Utils_MembersRateSql::getMembersRateSqlTitles();
      //$this->add('select', 'vat_rate_member_rate_sql_id', ts('Members Rate SQL'), array(''=>ts('- select -')) + $sqlArray , false );
// print_r($sqlArray);print_r($attributes['weight']);die;
      

		
		//memberes rate sql assigning ends here
		$this->addElement( 'text', 'excempt',  ts('Excempt'), array('size' => 15, 'maxlength' => 15), false );
		
	
        

        
     
       
        
        $this->addElement('hidden', 'action', $action );
        $this->addElement('hidden', 'id', $id );
        
        $this->addFormRule( array( 'CRM_Invoice_Form_VatCodeSetting', 'formRule' ) );
                               
		$this->addButtons(array( 
                                array ( 'type'      => 'next', 
                                        'name'      => ts('Save'), 
                                        'spacing'   => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 
                                        'isDefault' => false   ), 
                                ) 
                              );
    }
    	
    /**
     * global validation rules for the form
     *
     * @param array $fields posted values of the form
     *
     * @return array list of errors to be posted back to the form
     * @static
     * @access public
     */
    static function formRule( $values ) 
    {
        $errors = array( );
        if ($values['action'] == 'add') {
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." WHERE vat_code = %1";
            $dao = CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $values['vat_code'] , 'String' ),
                                                        ));
            if ($dao->fetch()) {
                $errors['vat_code'] = ts( "VAT and Invoice settings for ".$dao->vat_code." already added." );
            }
        }                   
        if ($values['action'] == 'update') {
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." WHERE vat_code = %1 AND id != %2";
            $dao = CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $values['vat_code'] , 'String' ),
                                                            2 => array( $values['id'] , 'Integer' ),
                                                        ));
            if ($dao->fetch()) {
                $errors['vat_code'] = ts( "VAT and Invoice settings for ".$dao->vat_code." already added." );
            }
        }                                     
        return $errors;
    }	
   
    /**
     * process the form after the input has been submitted and validated
     *
     * @access public
     * @return None
     */
    public function postProcess() {

		$params = $this->controller->exportValues( );
				
					if(!isset($params['vat_rate_member_rate_sql_id'])){
		$params['vat_rate_member_rate_sql_id'] = '';
		
		}
		if(!isset($params['vat_rate'])){
		$params['vat_rate'] = '';
		
		}
		
//var_dump($params);die;
        
       
           
        if(!empty($params['vat_code'])) {
    		if ($params['action'] == 'add') {
    		    $sql = "INSERT INTO ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." SET vat_code = %1 , vat_rate = %2 , zero_rate = %3,
                            vat_weighting = %4 , excempt = %5 , vat_rate_member_rate_sql_id = %6";
							
							//
							
							
							 //$sql = "INSERT INTO ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." SET contribution_type_id =" .$params['contribution_type_id'].", zero_rate = '".$params['zero_rate'] ."' , vat_rate = '".$params['vat_rate'] ."', vat_weighting = '".$params['vat_weighting']."', vat_code ='".$params['vat_code']."', nominal = '". $params['nominal']."', cost_centre = '".$params['cost_centre']."' , department = '". $params['department'] ."'";
							//var_dump($sql);die;
							//INSERT INTO civicrm_invoice_vatcode_setting SET contribution_type_id =4, zero_rate = '30' , vat_rate = '20', vat_weighting = '20', vat_code ='20', nominal = 'gfggg', cost_centre = 'gdfs' , department = 'df';
                CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $params['vat_code']  , 'String' ),
                                                            2 => array( $params['vat_rate']  , 'String' ),
                                                            3 => array( $params['zero_rate'] , 'String' ),
                                                            4 => array( $params['vat_weighting'] , 'String' ),
															5 => array( $params['excempt'] , 'String' ),
															6 => array( $params['vat_rate_member_rate_sql_id'] , 'String' ),
                                                        ));    
                $status = ts('Invoice settings added for <b></b>' );        
            } elseif ($params['action'] == 'update') {
                $sql = "UPDATE ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE." SET vat_code = %1 , vat_rate = %2 , zero_rate = %3,
                            vat_weighting = %4 , excempt = %5 , vat_rate_member_rate_sql_id = %6  WHERE id = %7";
                
                CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $params['vat_code']  , 'String' ),
                                                            2 => array( $params['vat_rate']  , 'String' ),
                                                            3 => array( $params['zero_rate'] , 'String' ),
                                                            4 => array( $params['vat_weighting'] , 'String' ),
															5 => array( $params['excempt'] , 'String' ),
															6 => array( $params['vat_rate_member_rate_sql_id'] , 'String' ),
														
															7 => array( $params['id'] , 'Integer' ),
                                                        ));
                $status = ts('Invoice settings updated for <b></b>' );        
            }
    		
        }
        CRM_Core_Session::setStatus($status);
        CRM_Utils_System::redirect(CRM_Utils_System::url( 'civicrm/admin/invoice/vatcode/setting', 'reset=1'));
        CRM_Utils_System::civiExit();
	}//end of function
}