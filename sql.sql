CREATE TABLE IF NOT EXISTS `civicrm_invoice_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
  `msg_template_id` int(10) unsigned NULL ,
  `display_vat_element` int(10) unsigned NULL ,
  `vat_percentage` int(10) unsigned  NULL ,
  `is_vat_inclusive` int(10) unsigned NULL ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO civicrm_invoice_setting(id, msg_template_id , display_vat_element , vat_percentage , is_vat_inclusive)
VALUES (1 , NULL , NULL , NULL , NULL); 