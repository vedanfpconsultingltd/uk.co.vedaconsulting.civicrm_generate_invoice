<?php
/*
+--------------------------------------------------------------------+
| CiviCRM version 4.1                                                |
+--------------------------------------------------------------------+
| Copyright CiviCRM LLC (c) 2004-2011                                |
+--------------------------------------------------------------------+
| This file is a part of CiviCRM.                                    |
|                                                                    |
| CiviCRM is free software; you can copy, modify, and distribute it  |
| under the terms of the GNU Affero General Public License           |
| Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
|                                                                    |
| CiviCRM is distributed in the hope that it will be useful, but     |
| WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
| See the GNU Affero General Public License for more details.        |
|                                                                    |
| You should have received a copy of the GNU Affero General Public   |
| License and the CiviCRM Licensing Exception along                  |
| with this program; if not, contact CiviCRM LLC                     |
| at info[AT]civicrm[DOT]org. If you have questions about the        |
| GNU Affero General Public License or the licensing of CiviCRM,     |
| see the CiviCRM license FAQ at http://civicrm.org/licensing        |
+--------------------------------------------------------------------+
*/
/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2011
 * $Id$
 *
 */
require_once 'CRM/Core/DAO.php';
require_once 'CRM/Utils/Type.php';

class CRM_Invoice_DAO_Setting extends CRM_Core_DAO
{
    /**
     * static instance to hold the table name
     *
     * @var string
     * @static
     */
    static $_tableName = CIVICRM_INVOICE_SETTINGS_TABLE;
    /**
     * static instance to hold the field values
     *
     * @var array
     * @static
     */
    static $_fields = null;
    /**
     * static instance to hold the FK relationships
     *
     * @var string
     * @static
     */
    static $_links = null;
    /**
     * static instance to hold the values that can
     * be imported / apu
     *
     * @var array
     * @static
     */
    static $_import = null;
    /**
     * static instance to hold the values that can
     * be exported / apu
     *
     * @var array
     * @static
     */
    static $_export = null;
    /**
     * static value to see if we should log any modifications to
     * this table in the civicrm_log table
     *
     * @var boolean
     * @static
     */
    static $_log = false;
    /**
     * Unique ID
     *
     * @var int unsigned
     */
    public $id;
    
    /**
     * Contribution Type Ids
     *
     * @var int unsigned
     */
    public $financial_type_id;
    
    /**
     * Message Template Id
     *
     * @var int unsigned
     */
    public $msg_template_id;
    /**
     * Is Display VAT?
     *
     * @var int unsigned
     */
    public $display_vat_element;
    /**
     * VAT Rate %
     *
     * @var int unsigned
     */
    public $vat_code;
    /**
     * VAT Code %
     *
     * @var varchar
     */
    public $nominal;
    /**
     * Is Total amount inclusive of VAT?
     *
     * @var varchar
     */
	  public $cost_centre;
    /**
     * Cost Centre
     *
     * @var varchar
     */
    public $department;
    /**
     * Department
     *
     * @var varchar
     */
    public $calculate_vat;
    public $is_vat_inclusive;
    public $create_pdf_invoice;
    public $attach_to_email;
    
    function __construct()
    {
        parent::__construct();
    }
    /**
     * return foreign links
     *
     * @access public
     * @return array
     */
    function &links()
    {
        return self::$_links;
    }
    /**
     * returns all the column names of this table
     *
     * @access public
     * @return array
     */
    static function &fields()
    {
        if (!(self::$_fields)) {
            self::$_fields = array(
                'id' => array(
                    'name' => 'id',
                    'type' => CRM_Utils_Type::T_INT,
                    'required' => true,
                ) ,
                'financial_type_id' => array(
                    'name' => 'financial_type_id',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts('Contribution Type IDs') ,
                    'size' => CRM_Utils_Type::HUGE,
                ) ,
                'msg_template_id' => array(
                    'name' => 'msg_template_id',
                    'type' => CRM_Utils_Type::T_INT,
                    'title' => ts(' Message Template ID') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'display_vat_element' => array(
                    'name' => 'display_vat_element',
                    'type' => CRM_Utils_Type::T_INT,
                    'title' => ts(' Display VAT?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'vat_code' => array(
                    'name' => 'vat_code',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' VAT Code') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
				'nominal' => array(
                    'name' => 'nominal',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' nominal') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'cost_centre' => array(
                    'name' => 'cost_centre',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Cost Centre') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
				 'department' => array(
                    'name' => 'department',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Department') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'calculate_vat' => array(
                    'name' => 'calculate_vat',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Calculate Vat?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'is_vat_inclusive' => array(
                    'name' => 'is_vat_inclusive',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Is VAT Inclusive?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'create_pdf_invoice' => array(
                    'name' => 'create_pdf_invoice',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Create PDF Invoice?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
                'attach_to_email' => array(
                    'name' => 'attach_to_email',
                    'type' => CRM_Utils_Type::T_STRING,
                    'title' => ts(' Attach Invoice to Email?') ,
                    'size' => CRM_Utils_Type::BIG,
                ) ,
            );
        }
        return self::$_fields;
    }
    /**
     * returns the names of this table
     *
     * @access public
     * @return string
     */
    static function getTableName()
    {
        return CRM_Core_DAO::getLocaleTableName(self::$_tableName);
    }
}
