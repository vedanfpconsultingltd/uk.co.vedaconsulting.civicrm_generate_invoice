{*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.1                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*}
<div class="crm-block crm-form-block crm-export-form-block">

<!--<div class="help">
<p>{ts}Use this form for Invoice Settings.{/ts}</p>
</div>-->

{if $action eq '1' OR $action eq '2'}
<div class="help">
    {ts}Use this form to add/edit VAT and Invoice settings for a contribution type.{/ts}
</div>
<table class="form-layout">
     <tr>
		<td>
			<table class="form-layout">
                <tr>
                    <td class="label">{$form.financial_type_id.label}</td>
                    <td>{$form.financial_type_id.html}
                    <br />
            		  <!--<span class="description"> 
            		  {ts}Please specify the contribution types, for which the invoice needs to be generated.{/ts}
            		  </span>-->
                    </td>
                <tr>
                <tr>
                    <td class="label">{$form.calculate_vat.label}</td>
                    <td>{$form.calculate_vat.html}
                    <br />
            		  <span class="description"> 
            		  {ts}Do you need to calculate VAT for this contribution type?{/ts}
            		  </span>
                    </td>
                <tr>
                
                <tr id="vat_rows">
                    <td colspan="2">
                        <div class="help">
                        <table class="form-layout">
                      
					      <tr id="vat_code_row">
                            <td class="label">{$form.vat_code.label}</td>
                            <td>{$form.vat_code.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the VAT Code. i.e. VAT applied to 50% or 100% of the CiviCRM Contribution Amount{/ts}
        		                </span>
                            </td>
                        </tr>

						  <!-- <tr id="vat_percentage_row">
                            <td class="label">{$form.vat_percentage.label}</td>
                            <td>{$form.vat_percentage.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the VAT Rate(%).{/ts}
        		                </span>
                            </td>
                        </tr>
                        <tr id="vat_weighting_row">
                            <td class="label">{$form.vat_weighting.label}</td>
                            <td>{$form.vat_weighting.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the VAT Weighting(%). i.e. VAT applied to 50% or 100% of the CiviCRM Contribution Amount{/ts}
        		                </span>
                            </td>
                        </tr> -->
                        
                        
                        
                        
                        
                        
                        <tr id="vat_inclusive">
                            <td class="label">{$form.is_vat_inclusive.label}</td>
                            <td>{$form.is_vat_inclusive.html} &nbsp;Yes
                                <br />
                    		    <span class="description"> 
                    		    {ts}Is the Amount in CiviCRM Contribution, inclusive of VAT? Do not tick, if the Amount in CiviCRM Contribution is Net.{/ts}
                    		    </span>
                            </td>
                        </tr>    
                        </table>
                        </div>    
                    </td>        
                </tr>
                <tr>
                <tr>
                
                
                <!--sri code starts here on 8th April 2013                
				<tr id="accounting_rows">
					<td colspan="2">
                        <div class="help">
                        <table class="form-layout">
						<tr>
						<tr><td colspan="2"><h3>Accounting Codes</h3></td></tr>
                        <tr id="nominal_row">
                            <td class="label">{$form.nominal.label}</td>
                            <td>{$form.nominal.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the Nominal{/ts}
        		                </span>
                            </td>
                        </tr>
                        
                        <tr id="cost_centre_row">
                            <td class="label">{$form.cost_centre.label}</td>
                            <td>{$form.cost_centre.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the Cost Centre{/ts}
        		                </span>
                            </td>
                        </tr>
                        
                               <tr id="department_row">
                            <td class="label">{$form.department.label}</td>
                            <td>{$form.department.html}
                                <br />
        		                <span class="description"> 
        		                {ts}Please specify the department{/ts}
        		                </span>
                            </td>
                        </tr>
                       </table>
                        </div>    
                    </td>        
                </tr>  -->
                          
                        
                        
                        
                        <!--sri code ends here on 8th April 2013-->
                
                <tr>
                    <td class="label">{$form.create_pdf_invoice.label}</td>
                    <td>{$form.create_pdf_invoice.html}
                    <br />
            		  <span class="description"> 
            		  {ts}Do you need to create a PDF invoices for the contributions of this type?<br />Enabling this option will create a PDF invoice when the contribution is created and the PDF invoices will be saved under 'Invoice Details' custom data group.<br />You can re-create invoices at a later stage, when you edit the contribution.{/ts}
            		  </span>
                    </td>
                <tr>
                
                <tr id="invoice_rows">
                    <td colspan="2">
                        <div class="help">
                        <table class="form-layout">
                        <tr id="msg_template_id_row">
                            <td class="label">{$form.msg_template_id.label}</td>
                            <td><b>{$form.msg_template_id.html}</b>
                            <br />
                    		  <span class="description"> 
                    		  {ts}Message template which will be used as Invoice template.{/ts}
                    		  </span>
                            </td>
                        <tr>
        				<tr id="display_vat_element_row">
                            <td class="label">{$form.display_vat_element.label}</td>
                            <td>{$form.display_vat_element.html} &nbsp;Yes
                                <br />
                      		    <span class="description"> 
                      		    {ts}Do you need the VAT elements to be displayed in the Invoice?{/ts}
                      		    </span>
                            </td>
                        </tr>
                        <tr id="attach_to_email_row">
                            <td class="label">{$form.attach_to_email.label}</td>
                            <td>{$form.attach_to_email.html} &nbsp;Yes
                                <br />
                      		    <span class="description"> 
                      		    {ts}Do you need to attach the Invoice to confirmation email?{/ts}
                      		    </span>
                            </td>
                        </tr>
                       </table>
                        </div>    
                    </td>        
                </tr> 
			</table>
		</td>
     </tr>
</table>
<div class="crm-submit-buttons">
  {include file="CRM/common/formButtons.tpl" location="top"}
</div>
</div>

{else if $action eq '8'}
    <h3>Delete invoice settings for {$contribution_type}</h3>
		<div class="crm-participant-form-block-delete messages status">
        <div class="crm-content">
            <div class="icon inform-icon"></div> &nbsp;
            {ts}WARNING: This operation cannot be undone.{/ts} {ts}Do you want to continue?{/ts}
        </div>
    </div>
    {assign var=id value=$id}
    <div class="crm-submit-buttons">
        <a class="button" href="{crmURL p='civicrm/admin/invoice/setting/add' q="action=force_delete&id=$id&reset=1"}"><span><div class="icon delete-icon"></div>{ts}Delete{/ts}</span></a>        
        <a class="button" href="{crmURL p='civicrm/admin/invoice/setting' q="reset=1"}"><span>{ts}Cancel{/ts}</span></a>
    </div>    
    </div>
{/if}


{literal}
<script type="text/javascript">
cj(document).ready( function() {
    cj('#calculate_vat').click(function () {
        displayVatRows();
        
        if (cj('#calculate_vat').is(':checked')) {
            cj("#display_vat_element").removeAttr("disabled");
        } else {
            cj("#display_vat_element").attr('checked', false);
            cj("#display_vat_element").attr("disabled", true);
        }
        
    });
    
    cj('#create_pdf_invoice').click(function () {
        displayInvoiceRows();     
    });
});

displayVatRows();
displayInvoiceRows();

function displayVatRows() {
    if (cj('#calculate_vat').is(':checked')) {
        //cj("#vat_percentage_row").show();
        //cj("#vat_weighting_row").show();
        //cj("#vat_inclusive").show();
        cj("#vat_rows").show();
    } else {
        //cj("#vat_percentage_row").hide();
        //cj("#vat_weighting_row").hide();
        //cj("#vat_inclusive").hide();
        cj("#vat_rows").hide();
    }
}


function displayInvoiceRows() {
    if (cj('#create_pdf_invoice').is(':checked')) {
        cj("#invoice_rows").show();
    } else {
        cj("#invoice_rows").hide();
    }
    
    if (cj('#calculate_vat').is(':checked')) {
        // do nothing
    } else {
        cj("#display_vat_element").attr("disabled", true);
    }
}

cj('#Setting').submit(function() {
        
    if(cj("#financial_type_id").val() == '') {
        alert('Please select a Contribution Type');
        return false;    
    }
        
    if(cj('#create_pdf_invoice').is(':checked') && cj("#msg_template_id").val() == '') {
        alert('Please select a message template, which will be used to create PDF invoice');
        return false;    
    }    
    
    if (cj("#create_pdf_invoice").prop('checked') == false && cj("#calculate_vat").prop('checked') == false) {
        alert('Please tick anyone of the provided options');
        return false;    
    }
    
    //if(!cj('#create_pdf_invoice').is(':checked') || !cj('#calculate_vat').is(':checked')') {
    //    alert('Please tick anyone of the provided options');
    //    return false;    
    //}
});



</script>
{/literal}
