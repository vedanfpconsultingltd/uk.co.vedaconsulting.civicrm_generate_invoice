<?php

/*
 +--------------------------------------------------------------------+
 | CiviCRM version 4.1                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2010                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007 and the CiviCRM Licensing Exception.   |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License and the CiviCRM Licensing Exception along                  |
 | with this program; if not, contact CiviCRM LLC                     |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2010
 * $Id$
 *
 */

require_once 'CRM/Core/Form.php';
require_once 'CRM/Core/Session.php';
require_once 'CRM/Contribute/PseudoConstant.php';

/**
 * This class provides the functionality for invoice settings
 */

class CRM_Invoice_Form_Setting extends CRM_Core_Form {

    /**
     * build all the data structures needed to build the form
     *
     * @return void
     * @access public
     */
	function preProcess()
  {	
        parent::preProcess( );
		    
	}
	
    /**
     * Build the form
     *
     * @access public
     * @return void
     */
    function buildQuickForm( ) {
       
        $action = CRM_Utils_Array::value('action', $_REQUEST, '');
        if (empty($action))
            $action = CRM_Utils_Array::value('action', $_POST, '');
        
        $id = CRM_Utils_Request::retrieve( 'id', 'Integer', $this );
        if (empty($id))
            $id = CRM_Utils_Array::value('id', $_POST, '');

        CRM_Utils_System::setTitle( 'Invoice Settings' );
//        CRM_Utils_System::setTitle( 'VAT and Invoice Settings' );

        $sql_params  = array( 1 => array( $id , 'Integer' ),);

        if ($action == 'update') {
        
            CRM_Utils_System::setTitle( 'Edit VAT and Invoice settings' );
        
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_SETTINGS_TABLE." WHERE id = %1";
            $dao = CRM_Core_DAO::executeQuery($sql , $sql_params);
            if($dao->fetch()) {
                        $defaults = array(
                          'financial_type_id'    => $dao->financial_type_id,
                          'msg_template_id'         => $dao->msg_template_id ,
                          'display_vat_element'     => $dao->display_vat_element ,
                          //'vat_percentage'          => $dao->vat_percentage ,
                          'is_vat_inclusive'        => $dao->is_vat_inclusive ,
                          //'vat_weighting'           => $dao->vat_weighting ,
						  'vat_code'        	    => $dao->vat_code ,
						  'nominal'        		    => $dao->nominal ,
						  'cost_centre'             => $dao->cost_centre ,
						  'department'   	        => $dao->department ,
                          'create_pdf_invoice'      => $dao->create_pdf_invoice ,
                          'calculate_vat'           => $dao->calculate_vat ,
                          'attach_to_email'         => $dao->attach_to_email ,
                          );
            }
            $this->setDefaults( $defaults );
            
        } elseif ($action == 'add') {
        
            CRM_Utils_System::setTitle( 'Add Invoice settings' );
//            CRM_Utils_System::setTitle( 'Add VAT and Invoice settings' );
            
        } elseif ($action == 'delete') {
        
            $this->assign('id', $id );
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_SETTINGS_TABLE." WHERE id = %1";
            $dao = CRM_Core_DAO::executeQuery($sql , $sql_params);
            $dao->fetch();
            $this->assign('contribution_type', CRM_Contribute_PseudoConstant::financialType($dao->financial_type_id));
            CRM_Utils_System::setTitle( 'Delete VAT and Invoice settings' );
            
        } elseif ($action == 'force_delete') {
        
            $sql = "DELETE FROM ".CIVICRM_INVOICE_SETTINGS_TABLE." WHERE id = %1";
    		CRM_Core_DAO::executeQuery($sql , $sql_params);
            
            $session = CRM_Core_Session::singleton( );
            $status = ts('Invoice setting deleted');
            CRM_Core_Session::setStatus($status);
            CRM_Utils_System::redirect(CRM_Utils_System::url( 'civicrm/admin/invoice/setting', 'reset=1'));
            CRM_Utils_System::civiExit();
        }
		
		
        //select VAT Code list
        $vat_code_array = array();
		$sql = "SELECT * FROM ".CIVICRM_INVOICE_VATCODE_SETTINGS_TABLE;  
		$dao = CRM_Core_DAO::executeQuery($sql);
        while($dao->fetch()){		 
            $vat_code_array[$dao->vat_code] = $dao->vat_code;		   
        }
        
        //$this->add('select', 'financial_type_id', ts('Contribution Type'), array('- select -') + CRM_Contribute_PseudoConstant::financialType(), TRUE);
        $this->add('select', 'financial_type_id', ts('Contribution Type'), array(''=>ts( '- select -' )) + CRM_Contribute_PseudoConstant::financialType( ), true );
        
        $this->addElement( 'checkbox', 'calculate_vat', ts( 'Calculate VAT?' ) , null , null );
        // $this->add( 'text', 'vat_percentage', ts('VAT Rate (%)'), array('size' => 5, 'maxlength' => 5), false );
        //$this->add( 'text', 'vat_weighting', ts('VAT Weighting (%)'), array('size' => 6, 'maxlength' => 6), false );
    
		//<!-- sri code starts here on 8th April -->
		$this->add('select', 'vat_code', ts('Vat Code'), array(''=>ts( '- select -' )) + $vat_code_array, false );
		
		$this->addElement( 'text', 'nominal',  ts('Nominal'), array('size' => 20, 'maxlength' => 15), false );
		$this->addElement( 'text', 'cost_centre',  ts('Cost Centre'), array('size' => 20, 'maxlength' => 20), false );
		$this->addElement( 'text', 'department',  ts('Department'), array('size' => 20, 'maxlength' => 20), false );
		
		//<!--sri code on ends here 8th April -->
	
        $this->addElement( 'checkbox', 'is_vat_inclusive', ts( 'Is CiviCRM amount Gross?' ) , null , null );
        
        $this->addElement( 'checkbox', 'create_pdf_invoice', ts( 'Create PDF Invoice?' ) , null , null );
        
        $msgTemplates = array();
        $template_sql = "SELECT id , msg_title FROM civicrm_msg_template WHERE workflow_id IS NULL";
		$template_dao = CRM_Core_DAO::executeQuery($template_sql);
        while ($template_dao->fetch()){
            $msgTemplates[$template_dao->id] = $template_dao->msg_title;
        }
        
        $msg_template_element = $this->add('select', 'msg_template_id', ts('Message Template'), array(''=>ts( '- select -' )) + $msgTemplates, false);
        
        //get the invoive message template id
        $msg_template_id = civicrm_generate_invoice_civicrm_get_invoice_message_template();
        if ($msg_template_id)
        {
            $defaults['msg_template_id'] = $msg_template_id;
            $this->setDefaults( $defaults );
            $msg_template_element->freeze();
        }
        
        $this->addElement( 'checkbox', 'display_vat_element', ts( 'Display VAT in Invoice?' ) , null , null );
        $this->addElement( 'checkbox', 'attach_to_email', ts( 'Attach Invoice to confirmation email?' ) , null , null );
        
        $this->addElement('hidden', 'action', $action );
        $this->addElement('hidden', 'id', $id );
        
        $this->addFormRule( array( 'CRM_Invoice_Form_Setting', 'formRule' ) );
                               
		$this->addButtons(array( 
                                array ( 'type'      => 'next', 
                                        'name'      => ts('Save'), 
                                        'spacing'   => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', 
                                        'isDefault' => false   ), 
                                ) 
                              );
    }
    	
    /**
     * global validation rules for the form
     *
     * @param array $fields posted values of the form
     *
     * @return array list of errors to be posted back to the form
     * @static
     * @access public
     */
    static function formRule( $values ) 
    {
        $errors = array( );
        if ($values['action'] == 'add') {
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_SETTINGS_TABLE." WHERE financial_type_id = %1";
            $dao = CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $values['financial_type_id'] , 'Integer' ),
                                                        ));
            if ($dao->fetch()) {
                $errors['financial_type_id'] = ts( "VAT and Invoice settings for ".CRM_Contribute_PseudoConstant::financialType($values['financial_type_id'])." already added." );
            }
        }                   
        if ($values['action'] == 'update') {
            $sql = "SELECT * FROM ".CIVICRM_INVOICE_SETTINGS_TABLE." WHERE financial_type_id = %1 AND id != %2";
            $dao = CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $values['financial_type_id'] , 'Integer' ),
                                                            2 => array( $values['id'] , 'Integer' ),
                                                        ));
            if ($dao->fetch()) {
                $errors['financial_type_id'] = ts( "VAT and Invoice settings for ".CRM_Contribute_PseudoConstant::financialType($values['financial_type_id'])." already added." );
            }
        }                                     
        return $errors;
    }	
   
    /**
     * process the form after the input has been submitted and validated
     *
     * @access public
     * @return None
     */
    public function postProcess() {

		$params = $this->controller->exportValues( );
        
          if (isset($params['calculate_vat'])) { 
		    $calculate_vat = 1;
            
            if (isset($params['is_vat_inclusive'])) { 
    		    $is_vat_inclusive = 1;
    		} else {
                $is_vat_inclusive = '';
            }
                
           /* if ($params['vat_percentage'] == '') { 
    		    $params['vat_percentage'] = 20; 
    		}
            
            if ($params['vat_weighting'] == '') { 
    		    $params['vat_weighting'] = 100; 
    		}*/
			
            
		} else {
            $calculate_vat = '';
            $is_vat_inclusive = '';
           // $params['vat_percentage'] = '';
            $params['vat_weighting'] = '';
			$params['vat_code'] = '';
			$params['nominal'] = '';
			$params['cost_centre'] = '';
			$params['department'] = '';
        } 
        
        if (isset($params['create_pdf_invoice'])) { 
		    $create_pdf_invoice = 1;
            
            if (isset($params['display_vat_element'])) { 
    		    $display_vat_element = 1;
    		} else {
                $display_vat_element = '';
            }
            
            if (isset($params['attach_to_email'])) { 
    		    $attach_to_email = 1;
    		} else {
                $attach_to_email = '';
            }
            
            $msg_template_id = $params['msg_template_id'];    
		} else {
            $msg_template_id = '';
            $create_pdf_invoice = '';
            $display_vat_element = '';
            $attach_to_email = '';
        }
           
        if(!empty($params['financial_type_id'])) {
    		if ($params['action'] == 'add') {
    		    $sql = "INSERT INTO ".CIVICRM_INVOICE_SETTINGS_TABLE." SET financial_type_id = %1 , msg_template_id = %2 , display_vat_element = %3 , vat_code = %6 , nominal = %7 , cost_centre = %8 , department = %9 ,is_vat_inclusive = %10 , calculate_vat = %11 , create_pdf_invoice = %12 , attach_to_email = %13";
                CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $params['financial_type_id'] , 'Integer' ),
                                                            2 => array( $msg_template_id , 'String' ),
                                                            3 => array( $display_vat_element , 'String' ),
                                                            //4 => array( $params['vat_percentage'] , 'String' ),
                                                           // 5 => array( $params['vat_weighting'] , 'String' ),
															6 => array( $params['vat_code'] , 'String' ),
															7 => array( $params['nominal'] , 'String' ),
															8 => array( $params['cost_centre'] , 'String' ),
															9 => array( $params['department'] , 'String' ),
                                                            10 => array( $is_vat_inclusive , 'String' ),
                                                            11 => array( $calculate_vat , 'String' ),
                                                            12 => array( $create_pdf_invoice , 'String' ),
                                                            13 => array( $attach_to_email , 'String' ),
                                                        ));    
                $status = ts('Invoice settings added for <b>'.CRM_Contribute_PseudoConstant::financialType($params['financial_type_id']).'</b>' );        
            } elseif ($params['action'] == 'update') {
                $sql = "UPDATE ".CIVICRM_INVOICE_SETTINGS_TABLE." SET financial_type_id = %1 , msg_template_id = %2 , display_vat_element = %3 , vat_code = %6 , nominal = %7 , cost_centre = %8 , department = %9 ,is_vat_inclusive = %10 , calculate_vat = %11 , create_pdf_invoice = %12 , attach_to_email = %13 WHERE id = %14";
                
                CRM_Core_DAO::executeQuery($sql , array( 
                                                            1 => array( $params['financial_type_id'] , 'Integer' ),
                                                            2 => array( $msg_template_id , 'String' ),
                                                            3 => array( $display_vat_element , 'String' ),
                                                           // 4 => array( $params['vat_percentage'] , 'String' ),
                                                           //5 => array( $params['vat_weighting'] , 'String' ),
                                                            6 => array( $params['vat_code'] , 'String' ),
															7 => array( $params['nominal'] , 'String' ),
															8 => array( $params['cost_centre'] , 'String' ),
															9 => array( $params['department'] , 'String' ),
                                                            10 => array( $is_vat_inclusive , 'String' ),
                                                            11 => array( $calculate_vat , 'String' ),
                                                            12 => array( $create_pdf_invoice , 'String' ),
                                                            13 => array( $attach_to_email , 'String' ),
															14 => array( $params['id'] , 'Integer' ),
                                                        ));
                $status = ts('Invoice settings updated for <b>'.CRM_Contribute_PseudoConstant::financialType($params['financial_type_id']).'</b>' );        
            }
    		
        }
        CRM_Core_Session::setStatus($status);
        CRM_Utils_System::redirect(CRM_Utils_System::url( 'civicrm/admin/invoice/setting', 'reset=1'));
        CRM_Utils_System::civiExit();
	}//end of function
}